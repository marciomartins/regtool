program RegTool;
{
Todo:
Parametrizar o RootKey
Adicionr comandos com insert, delete
}

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, System.Win.Registry, Winapi.Windows;

procedure Update(const Key, Name, Value: string);
var
  Reg: TRegistry;
  OldValue: string;
begin
  Reg := TRegistry.Create(KEY_ALL_ACCESS or KEY_WOW64_32KEY);
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.OpenKey(Key, False) then
    begin
      OldValue := Reg.ReadString(Name);
      Reg.WriteString(Name, OldValue+Value);
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

var
  Command: string;

begin
  try
    if ParamCount <= 2 then
    begin
      Writeln('RegTool 1.0.0 By M�rcio Martins - 05/08/2014');
      Writeln('RegTool.exe update "Key" "Name" "NewValue"');
      Writeln('RegTool.exe update "HKEY_CURRENT_USER\Software\Embarcadero\BDS\14.0\Library" "Search Path" ";C:\Temp"');
    end
    else
    begin
      Command := ParamStr(1);
      if SameText(Command, 'UPDATE') then
        Update(ParamStr(2), ParamStr(3), ParamStr(4))
      else
        raise Exception.Create('Comando n�o implementado.');
    end;
    { TODO -oUser -cConsole Main : Insert code here }
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
